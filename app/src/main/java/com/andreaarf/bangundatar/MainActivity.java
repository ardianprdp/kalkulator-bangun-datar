package com.andreaarf.bangundatar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    String[] items = {"Persegi", "Persegi Panjang", "Lingkaran", "Segitiga", "Belah Ketupat", "Trapesium", "Jajar Genjang", "Layang Layang"};
    String[] refItems = {"persegi", "persegipanjang", "lingkaran", "segitiga", "belahketupat", "trapesium", "jajargenjang", "layanglayang"};
    private AutoCompleteTextView ddBidangDatar;
    private ArrayAdapter<String> arrayAdapter;
    private CardView containerCard;
    private Button btnHitung;
    private EditText sisiPersegi, panjangPersegiPanjang, lebarPersegiPanjang, jarijariLingkaran, alasSegitiga, tinggiSegitiga, diagonal1BelahKetupat, diagonal2BelahKetupat, sisi1Trapesium, sisi2Trapesium, tinggiTrapesium, alasJajarGenjang, tinggiJajarGenjang, diagonal1LayangLayang, diagonal2LayangLayang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        arrayAdapter = new ArrayAdapter<String>(this, R.layout.list_item, items);


        ddBidangDatar = findViewById(R.id.ddBidang);
        ddBidangDatar.setAdapter(arrayAdapter);

        ddBidangDatar.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();
                for (String dataItem: refItems) {
                    Resources res = getResources();
                    int idCard = res.getIdentifier(dataItem, "id", getApplicationContext().getPackageName());
                    containerCard = findViewById(idCard);
                    containerCard.setVisibility(View.GONE);
                }
                Resources res = getResources();
                int idCardClick = res.getIdentifier(refItems[i], "id", getApplicationContext().getPackageName());
                containerCard = findViewById(idCardClick);
                containerCard.setVisibility(View.VISIBLE);

                Resources resBtn = getResources();
                int btnId = resBtn.getIdentifier(refItems[i]+"HitungBtn", "id", getApplicationContext().getPackageName());
                btnHitung = findViewById(btnId);

                btnHitung.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        switch (i){
                            case 0:
                                persegiLuas(item);
                                break;
                            case 1:
                                persegipanjangLuas(item);
                                break;
                            case 2:
                                lingkaranLuas(item);
                                break;
                            case 3:
                                segitigaLuas(item);
                                break;
                            case 4:
                                belahketupatLuas(item);
                                break;
                            case 5:
                                trapesiumLuas(item);
                                break;
                            case 6:
                                jajargenjangLuas(item);
                                break;
                            case 7:
                                layanglayangLuas(item);
                                break;
                        }
                    }
                });

            }
        });
    }

    public void persegiLuas(String item){
        sisiPersegi = findViewById(R.id.sisiPersegi);
        String sisi = sisiPersegi.getText().toString();
        String hasil;
        if(!sisi.equals("")){
            Double s = Double.parseDouble(sisi);
            Double luas = s * s;
            hasil = String.valueOf(luas);
        }else{
            hasil = null;
        }
        showResult(hasil, item);
    }

    public void persegipanjangLuas(String item){
        panjangPersegiPanjang = findViewById(R.id.panjangPersegiPanjang);
        lebarPersegiPanjang = findViewById(R.id.lebarPersegiPanjang);
        String panjang = panjangPersegiPanjang.getText().toString();
        String lebar = lebarPersegiPanjang.getText().toString();
        String hasil;
        if(!panjang.equals("") && !lebar.equals("")){
            Double p = Double.parseDouble(panjang);
            Double l = Double.parseDouble(lebar);
            Double luas = p * l;
            hasil = String.valueOf(luas);
        }else{
            hasil = null;
        }
        showResult(hasil, item);
    }

    public void lingkaranLuas(String item){
        jarijariLingkaran = findViewById(R.id.jarijariLingkaran);
        String jarijari = jarijariLingkaran.getText().toString();
        String hasil;
        if(!jarijari.equals("")){
            Double r = Double.parseDouble(jarijari);
            Double luas =  r * r * 22 / 7;
            hasil = String.valueOf(luas);
        }else{
            hasil = null;
        }
        showResult(hasil, item);
    }

    public void segitigaLuas(String item){
        alasSegitiga = findViewById(R.id.alasSegitiga);
        tinggiSegitiga = findViewById(R.id.tinggiSegitiga);
        String alas = alasSegitiga.getText().toString();
        String tinggi = tinggiSegitiga.getText().toString();
        String hasil;
        if(!alas.equals("") && !tinggi.equals("")){
            Double a = Double.parseDouble(alas);
            Double t = Double.parseDouble(tinggi);
            Double luas = a * t / 2;
            hasil = String.valueOf(luas);
        }else{
            hasil = null;
        }
        showResult(hasil, item);
    }

    public void belahketupatLuas(String item){
        diagonal1BelahKetupat = findViewById(R.id.diagonal1BelahKetupat);
        diagonal2BelahKetupat = findViewById(R.id.diagonal2BelahKetupat);
        String diagonal1 = diagonal1BelahKetupat.getText().toString();
        String diagonal2 = diagonal2BelahKetupat.getText().toString();
        String hasil;
        if(!diagonal1.equals("") && !diagonal2.equals("")){
            Double d1 = Double.parseDouble(diagonal1);
            Double d2 = Double.parseDouble(diagonal2);
            Double luas = d1 * d2 / 2;
            hasil = String.valueOf(luas);
        }else{
            hasil = null;
        }
        showResult(hasil, item);
    }

    public void trapesiumLuas(String item){
        sisi1Trapesium = findViewById(R.id.sisi1Trapesium);
        sisi2Trapesium = findViewById(R.id.sisi2Trapesium);
        tinggiTrapesium = findViewById(R.id.tinggiTrapesium);
        String sisi1 = sisi1Trapesium.getText().toString();
        String sisi2 = sisi2Trapesium.getText().toString();
        String tinggi = tinggiTrapesium.getText().toString();
        String hasil;
        if(!sisi1.equals("") && !sisi2.equals("") && !tinggi.equals("")){
            Double a = Double.parseDouble(sisi1);
            Double b = Double.parseDouble(sisi2);
            Double t = Double.parseDouble(tinggi);
            Double luas = ((a+b)/2) * t;
            hasil = String.valueOf(luas);
        }else{
            hasil = null;
        }
        showResult(hasil, item);
    }

    public void jajargenjangLuas(String item){
        alasJajarGenjang = findViewById(R.id.alasJajarGenjang);
        tinggiJajarGenjang = findViewById(R.id.tinggiJajarGenjang);
        String alas = alasJajarGenjang.getText().toString();
        String tinggi = tinggiJajarGenjang.getText().toString();
        String hasil;
        if(!alas.equals("") && !tinggi.equals("")){
            Double a = Double.parseDouble(alas);
            Double t = Double.parseDouble(tinggi);
            Double luas = a * t;
            hasil = String.valueOf(luas);
        }else{
            hasil = null;
        }
        showResult(hasil, item);
    }

    public void layanglayangLuas(String item){
        diagonal1LayangLayang = findViewById(R.id.diagonal1LayangLayang);
        diagonal2LayangLayang = findViewById(R.id.diagonal2LayangLayang);
        String diagonal1 = diagonal1LayangLayang.getText().toString();
        String diagonal2 = diagonal2LayangLayang.getText().toString();
        String hasil;
        if(!diagonal1.equals("") && !diagonal2.equals("")){
            Double d1 = Double.parseDouble(diagonal1);
            Double d2 = Double.parseDouble(diagonal2);
            Double luas = d1 * d2 / 2;
            hasil = String.valueOf(luas);
        }else{
            hasil = null;
        }
        showResult(hasil, item);
    }

    public void showResult(String result, String item){
        String judul, hasil;
        if(result != null){
            judul = "Hasil";
            hasil = "Luas "+item+" adalah "+result;
        }else{
            judul = "Isian Kosong";
            hasil = "Pastikan Anda telah mengisi isian dengan benar.";
        }

        AlertDialog.Builder ab = new AlertDialog.Builder(MainActivity.this);
        ab.setTitle(judul);
        ab.setMessage(hasil);
        ab.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = ab.create();
        dialog.show();
    }
}